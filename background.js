chrome.webNavigation.onHistoryStateUpdated.addListener(function (details) {

    var pull1 = ".*://github.houston.softwaregrp.net/.*/pull/.*/files";
    var pull2 = ".*://github.com/.*/pull/.*/files";


    var pull_commit1 = ".*://github.houston.softwaregrp.net/.*/pull/.*/commits/.*";
    var pull_commit2 = ".*://github.com/.*/pull/.*/commits/.*";


    if ((details.url.match(pull1) || details.url.match(pull2)) && !details.url.includes('#')) {
        chrome.tabs.query({ active: true, currentWindow: true }, function (arrayOfTabs) {
            var activeTab = arrayOfTabs[0];
            chrome.tabs.insertCSS(activeTab.id, { file: "./css/index.css" });
            chrome.tabs.insertCSS(activeTab.id, { file: "./css/bootstrap.css" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/jquery-3.4.1.min.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/bootstrap.min.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/Entry.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/PathObj.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/Functions.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./contentScript.js" });
        });
    }

    if ((details.url.match(pull_commit1) || details.url.match(pull_commit2)) && !details.url.includes('#')) {
        chrome.tabs.query({ active: true, currentWindow: true }, function (arrayOfTabs) {
            var activeTab = arrayOfTabs[0];
            chrome.tabs.insertCSS(activeTab.id, { file: "./css/index.css" });
            chrome.tabs.insertCSS(activeTab.id, { file: "./css/bootstrap.css" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/jquery-3.4.1.min.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/bootstrap.min.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/Entry.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/PathObj.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./js/Functions.js" });
            chrome.tabs.executeScript(activeTab.id, { file: "./contentScript.js" });
        });
    }

    var reviewdButton = "./*://github.com/pulls.*";
    var reviewdButton2 = "./*://github.houston.softwaregrp.net/pulls.*";

    if (details.url.match(reviewdButton) || details.url.match(reviewdButton2)) {
        chrome.tabs.query({ active: true, currentWindow: true }, function (arrayOfTabs) {
            var activeTab = arrayOfTabs[0];
            chrome.tabs.executeScript(activeTab.id, { file: "./reviewdButton.js" });

            if (details.url.includes('reviewed-by%3A')) {

                chrome.tabs.query({ active: true, currentWindow: true }, function (arrayOfTabs) {
                    chrome.tabs.executeScript(arrayOfTabs[0].id, { code: 'document.getElementById(\'reviewed-btn\').classList.add(\'selected\')' });
                });
            }
        });
    }
});

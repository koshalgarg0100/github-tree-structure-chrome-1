var openedClass = 'glyphicon-folder-open';
var closedClass = 'glyphicon-folder-close';
var allFiles;

function urlParam(url, name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
    return results[1] || 0;
}

function treed() {

    var tree = $('#tree');
    tree.addClass("tree");
    tree.find('li').has("ul").each(function () {
        var branch = $(this); //li with children ul
        branch.on('click', function (e) {
            if (this == e.target) {
                var icon = $(this).children('i:first');
                icon.toggleClass(openedClass + " " + closedClass);
                $(this).children().children().toggle();
            }
        })
    });
}

if (!document.getElementById('col1')) {
    main();
}



function main() {
    if (document.getElementById('col1'))
        return;


    var comp = setup();
    var root = comp['root'];
    var loader = comp['loader'];

    // Select the node that will be observed for mutations
    var targetNode = document.getElementById('files');

    var files = [];
    var initialFiles = targetNode.getElementsByClassName('link-gray-dark');

    for (let link of initialFiles) {

        var parentDiv = link.parentElement.parentElement;
        var fileType = parentDiv.parentElement.getAttribute('data-file-type');
        var isDeleted = parentDiv.parentElement.getAttribute('data-file-deleted');
        var path = parentDiv.getAttribute('data-path');
        var filelink = '#' + parentDiv.getAttribute('data-anchor');

        var isNew = checkIfIsNew(link);

        var cpath = new PathObj(path, filelink, fileType, isDeleted, isNew);

        files.push(cpath);
    }

    var data = new Directory(null, "root", -1, {});
    allFiles = files;
    createFolderStructure(files, data);
    populateData(data, root);
    treed();
    loader.style.display = 'none';
    addMutationObserver(files, data, root, loader);

}


function setup() {

    $('.container').css({ "width": "auto", "padding": "0 20px" });
    $('.container-lg').css({ "max-width": "none" });

    var mainContainer = document.getElementById('files');

    var d = document.createElement('div');
    // d.classList.add('container-fluid');
    d.classList.add('row');
    d.classList.add('ext-contents');
    d.style.padding = 0;
    d.style.margin = 0;
    d.id = 'div1';


    var col1 = document.createElement('div');
    var loader = getLoader();

    col1.appendChild(getSearchBar());
    col1.classList.add('col-sm-3');
    col1.classList.add('hidden-xs');
    // col1.classList.add('file-header');
    col1.id = 'col1';
    col1.style.padding = '5px 0';

    var he = window.innerHeight - 60;
    col1.style.height = he + 'px';

    var col2 = document.createElement('div');
    col2.id = 'col2';
    col2.classList.add('col-sm-9');
    col2.style.paddingRight = '0';
    col2.style.paddingLeft = '7px';

    var root = document.createElement('ul');
    root.id = 'tree';


    mainContainer.parentElement.insertBefore(d, mainContainer);

    col1.appendChild(root);
    col1.appendChild(loader);

    col2.appendChild(mainContainer);

    d.appendChild(col1);
    d.appendChild(col2);

    return { 'root': root, 'loader': loader };

}



function getSearchBar() {
    var bar = document.createElement('div');
    bar.classList.add('row');
    bar.style.marginLeft = "14px";

    var search = document.createElement('input');
    search.classList.add('col-sm-9');
    search.classList.add('form-control-ex');
    search.setAttribute('placeholder', "Search");
    search.id = "ext_search";

    var expand = document.createElement('button');
    expand.classList.add('glyphicon');
    expand.classList.add('glyphicon-plus');
    expand.classList.add('col-sm-1');
    expand.classList.add('bar-element');
    expand.classList.add('btn-ex');
    expand.setAttribute('data-toggle', "tooltip");
    expand.setAttribute('data-placement', "top");
    expand.setAttribute('title', "Expand all");
    expand.id = 'ext_expand';


    var collapse = document.createElement('button');
    collapse.classList.add('glyphicon');
    collapse.classList.add('glyphicon-minus');
    collapse.classList.add('col-sm-1');
    collapse.classList.add('bar-element');
    collapse.classList.add('btn-ex');
    collapse.setAttribute('data-toggle', "tooltip");
    collapse.setAttribute('data-placement', "top");
    collapse.setAttribute('title', "Collapse all");
    collapse.id = 'ext_collapse';


    bar.appendChild(search);
    bar.appendChild(expand);
    bar.appendChild(collapse);

    return bar;
}

$("#ext_collapse").click(function () {
    var tree = $('#tree');
    tree.find('li').has("ul").each(function () {
        var icon = $(this).children('i:first');
        if (icon.hasClass(openedClass)) {
            icon.addClass(closedClass).removeClass(openedClass);
            $(this).children().children().hide();
        }
    });
});

$("#ext_expand").click(function () {
    var tree = $('#tree');
    tree.find('li').has("ul").each(function () {
        var icon = $(this).children('i:first');
        if (icon.hasClass(closedClass)) {
            icon.addClass(openedClass).removeClass(closedClass);
            $(this).children().children().show();
        }
    });
});

$('#ext_search').on('input', function () {

    var key = $('#ext_search').val();

    var data = new Directory(null, "root", -1, {});
    var tree = document.getElementById("tree");

    var files = [];

    if (key == '') {
        files = allFiles;
    }
    else {

        for (let cur of allFiles) {
            if (cur['path'].search(new RegExp(key, "i")) != -1) {
                files.push(cur);
            }
        }

    }

    createFolderStructure(files, data);
    populateData(data, tree);
    treed();

});


function addMutationObserver(files, data, root, loader) {
    var targetNode = document.getElementById('files');

    var config = { childList: true, subtree: true };

    var callback = function (mutationsList, observer) {
        for (var mutation of mutationsList) {
            var addedNodes = mutation.addedNodes;

            for (var node of addedNodes) {
                if (node.nodeType == 1) {
                    var links = node.getElementsByClassName('link-gray-dark');
                    for (var link of links) {

                        var parentDiv = link.parentElement.parentElement;
                        var fileType = parentDiv.parentElement.getAttribute('data-file-type');
                        var isDeleted = parentDiv.parentElement.getAttribute('data-file-deleted');
                        var path = parentDiv.getAttribute('data-path');
                        var filelink = '#' + parentDiv.getAttribute('data-anchor');

                        var isNew = checkIfIsNew(link);

                        var cpath = new PathObj(path, filelink, fileType, isDeleted, isNew);
                        files.push(cpath);
                    }
                }
            }

        }


        allFiles = files;
        createFolderStructure(files, data);
        populateData(data, root);
        treed();
    };

    var observer = new MutationObserver(callback);
    observer.observe(targetNode, config);


}

function checkIfIsNew(link) {
    // var isNew = 'false';
    // var span = link.parentElement.getElementsByClassName('diffstat')[0];
    // var string = span.getAttribute('aria-label');
    // var numbers = string.match(/\d+/g).map(Number);

    // console.log(numbers);

    return 'false';

}


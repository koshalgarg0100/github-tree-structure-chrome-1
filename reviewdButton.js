var nav = document.getElementsByClassName('subnav-links')[0];
var userName = document.getElementsByName('user-login')[0].content;
if (nav.children.length < 5) {
    addBtn();

}

function addBtn() {


    var reviewed = document.createElement('a');

    reviewed.classList.add('js-selected-navigation-item');
    reviewed.classList.add('subnav-item');
    reviewed.setAttribute('href', '/pulls?q=is%3Aopen+is%3Apr+reviewed-by%3A' + userName);
    reviewed.id = 'reviewed-btn';


    reviewed.innerText = "Reviewed";
    nav.appendChild(reviewed);
    reviewed.setAttribute('Title', 'Pull Requests reviewed by you');

    reduceSearchBarSize(reviewed.offsetWidth);
    removeUrlP('+reviewed-by%3A' + userName);
}

function reduceSearchBarSize(offsetWidth) {
    var bar = document.getElementById('js-issues-search');
    bar.style.width = (bar.offsetWidth - offsetWidth) + 'px'
}

function removeUrlP(str) {

    for (var i = 0; i < 4; i++) {
        var oldurl = nav.children[i].getAttribute('href');
        var newurl = oldurl.replace(str, '');
        newurl = newurl.replace('+archived%3Afalse', '');
        nav.children[i].setAttribute('href', newurl);

    }
}

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}

class Entry {
    constructor(parent, name, level) {
        this.parent = parent;
        this.name = name;
        this.level = level;
    }
}


class Directory extends Entry {
    constructor(parent, name, level, entries) {
        super(parent, name, level);
        this.entries = entries;
    }
}

class File extends Entry {
    constructor(parent, name, level, url, extn, isDeleted, isNew) {
        super(parent, name, level);
        this.url = url;
        this.extn = extn;
        this.isDeleted = isDeleted;
        this.isNew = isNew;
    }
}
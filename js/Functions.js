function processData(DOM) {
    var files = [];
    var filesChangedDivs = DOM.getElementsByClassName('link-gray-dark');


    for (var i = 0; i < filesChangedDivs.length; i++) {

        // if (filesChangedDivs[i].classList.length == 1) {

        var cpath = new PathObj(filesChangedDivs[i].innerText, filesChangedDivs[i].href);
        files.push(cpath);
        // }
    }

    return files;
}


function createFolderStructure(pathObjects, root) {
    pathObjects.forEach(pathObj => {
        path = pathObj.path;

        var pathEntries = path.split("/");
        var fileName = pathEntries.pop();

        var parentLevel = root.level;
        var currentParent = root;

        pathEntries.forEach(folderName => {

            var folder = currentParent.entries[folderName];
            if (!folder) {
                folder = new Directory(currentParent.name, folderName, parentLevel + 1, {});
                currentParent.entries[folderName] = folder;
            }

            parentLevel = parentLevel + 1;
            currentParent = folder;
        });

        var newFil = new File(currentParent.name, fileName, parentLevel + 1, pathObj.url, pathObj.extn, pathObj.isDeleted, pathObj.isNew);
        currentParent.entries[fileName] = newFil;

    });
}

function populateData(data, root) {

    root.innerText = '';


    if (data == null)
        return;

    if (data instanceof File) {
        root.appendChild(getFileComponent(data.name, data.url, data.extn, data.isDeleted, data.isNew));
        return;
    }


    for (key in data.entries) {
        var entry = data.entries[key];

        if (entry instanceof File)
            root.appendChild(getFileComponent(entry.name, entry.url, entry.extn, entry.isDeleted, entry.isNew));

        else {
            var effective_folder_name = entry.name;

            while (Object.keys(entry.entries).length == 1) {
                var inEntry = entry.entries[Object.keys(entry.entries)[0]];
                if (inEntry instanceof File)
                    break;
                else {
                    effective_folder_name = effective_folder_name + '/' + inEntry.name;

                }
                entry = inEntry;
            }


            var folder = getFolderComponent(effective_folder_name);
            var folderData = document.createElement('ul');
            populateData(entry, folderData);
            folder.appendChild(folderData);
            root.appendChild(folder);
        }
    }
}

function getFileComponent(fileName, url, extn, isDeleted, isNew) {

    if (extn)
        extn = extn.split('.')[1] + '.png';

    var list = document.createElement("li");
    var a = document.createElement("a");
    var icon = document.createElement('img');

    icon.setAttribute('src', chrome.runtime.getURL('icon/16px/' + extn));

    icon.addEventListener("error", function () {
        this.setAttribute('src', chrome.runtime.getURL('icon/16px/_blank.png'));
    });

    icon.setAttribute('height', '16px');
    icon.setAttribute('width', '16px');
    icon.style.marginRight = '3px';

    a.appendChild(icon);
    a.append(fileName);

    if (isDeleted == 'true') {
        a.style.color = '#cb2431';
    }

    if (isNew == 'true') {
        a.style.color = '#2cbe4e';
    }

    a.setAttribute('href', url);
    list.appendChild(a);

    return list;
}

function getFolderComponent(fileName) {
    var folder = document.createElement("li");
    var icon = document.createElement('i');
    icon.classList.add('indicator');
    icon.classList.add('glyphicon');
    icon.classList.add('glyphicon-folder-open');

    folder.appendChild(icon);
    folder.append(fileName);

    return folder;
}


function getLoader() {
    var loader = document.createElement('img');
    loader.setAttribute('src', chrome.runtime.getURL("images/loader.gif"));
    loader.id = 'loader-f';
    return loader;
}